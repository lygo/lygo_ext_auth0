package lygo_ext_auth0

import "bitbucket.org/lygo/lygo_commons/lygo_json"

//----------------------------------------------------------------------------------------------------------------------
//	Auth0Response
//----------------------------------------------------------------------------------------------------------------------

type Auth0Response struct {
	Error        string                 `json:"error"`
	ItemId       string                 `json:"item_id"`
	ItemPayload  map[string]interface{} `json:"item_payload"`
	AccessToken  string                 `json:"access_token"`
	RefreshToken string                 `json:"refresh_token"`
	ConfirmToken string                 `json:"confirm_token"`
}

func (instance *Auth0Response) GoString() string {
	return lygo_json.Stringify(instance)
}

func (instance *Auth0Response) String() string {
	return lygo_json.Stringify(instance)
}

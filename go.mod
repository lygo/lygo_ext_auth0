module bitbucket.org/lygo/lygo_ext_auth0

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10 // indirect
	bitbucket.org/lygo/lygo_ext_dbbolt v0.1.3
	github.com/arangodb/go-driver v0.0.0-20210825071748-9f1169c6a7dc
	github.com/google/uuid v1.3.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/sys v0.0.0-20210909193231-528a39cd75f3 // indirect
)

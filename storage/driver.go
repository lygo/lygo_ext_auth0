package storage

import (
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"time"
)

type IDatabase interface {
	Enabled() bool
	Open() error
	Close() error
	EnableCache(value bool)
	AuthRegister(key, payload string) error
	AuthGet(key string) (string, error)
	AuthRemove(key string) error
	AuthOverwrite(key, payload string) error


	CacheGet(key string) (string, error)
	CacheRemove(key string) error
	CacheAdd(key, token string, duration time.Duration) error // add new or update existing
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func NewDatabase(driverName, connectionString string, isCache bool) (driver IDatabase, err error) {
	dsn := NewDsn(connectionString)
	if dsn.IsValid() {
		switch driverName {
		case "arango":
			driver = NewDriverArango(dsn)
			driver.EnableCache(isCache)
			err = driver.Open()
		case "bolt":
			driver = NewDriverBolt(dsn)
			driver.EnableCache(isCache)
			err = driver.Open()
		default:
			driver = nil
			err = ErrorDriverNotImplemented
		}
		return driver, err
	}
	return driver, err
}

func BuildKey(username, password string) string {
	return lygo_crypto.MD5(username + password)
}

func EncryptText(key []byte, value string) (string, error) {
	return lygo_crypto.EncryptTextWithPrefix(value, key)
}

func DecryptText(key []byte, value string) (string, error) {
	return lygo_crypto.DecryptTextWithPrefix(value, key)
}

func EncryptPayload(key []byte, value map[string]interface{}) (string, error) {
	json := lygo_json.Stringify(value)
	return EncryptText(key, json)
}

func DecryptPayload(key []byte, value string) (map[string]interface{}, error) {
	data, err := DecryptText(key, value)
	if nil != err {
		return nil, err
	}
	var e map[string]interface{}
	err = lygo_json.Read(data, &e)
	return e, err
}
